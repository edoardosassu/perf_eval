#awk -v out=[output_file_name] -v factorName=[name of the factor] -f [awk_script] [pat_to_.vec_files]/*.vec
#
#The script create a structure whit this format:
#
#data:{
#   [moduleName]:{
#   	[statisticName]:{
#           [factorName]_[factorsValues]:{
#               [fatctorName]: [factorsValues]
#               [statType]: [vector of values]
#           }
#		    ...
#       }
#   ...
#   }
#	...
#}
#factor values can be:
#-single value string (0.5 15 ...)
#-multivalue string. The value separator must be _ (15_20, 15_0.95)
#dots (.) are changed to -
#If favtorsValues is a multiple value string the fild [fatctorName]: [factorsValues]
#will be a matlab array with this format:
#   [factorName]_[value1]_[value2].._[valueN] = [ value1 value2 ... valueN ]
#Otherwise it will have this format:
#   [factorName]_value = value

#Setting up .m file
BEGIN{
    print "parseVec:\tInitialization"
    out_file = out".m"
    print "data = struct;" > out_file
}

#get current run number
//{
    factorsValues = $1
    factorsNum=split(factorsValues, factors, "_")
    structFactorName_Value=factorName "_" gensub(/\./, "", "g", factorsValues)
    moduleName=$2
    statisticName=$3
    statType = $4

    if(length(moduleNames[moduleName])==0){
        moduleNames[moduleName]=moduleName
        print "data."moduleName"= struct;" > out_file
    }

    if(length(statNames[moduleName,statisticName])==0){
        statNames[moduleName,statisticName]=statisticName
        print "data."moduleName"."statisticName" = struct;" > out_file
    }

    if(length(fact[moduleName,statisticName,structFactorName_Value])==0){
        currentStructString="data."moduleName"."statisticName"."structFactorName_Value;

        fact[moduleName,statisticName,structFactorName_Value]="structFactorName_Value";

        print "data."moduleName"."statisticName"."structFactorName_Value"=struct;">out_file;

        if(factorsNum==1){
            print currentStructString"."factorName" = "factorsValues";" > out_file
        }
        else{
            matlabFactorsValues=gensub(/_/," ", "g", factorsValues)
            print currentStructString"."factorName" = [ "matlabFactorsValues" ];" > out_file
        }

    }

    if(length(data[moduleName,statisticName,structFactorName_Value,statType])==0){
        print "data."moduleName"."statisticName"."structFactorName_Value"."statType"= [ ">out_file
    }

    for(i=5;i<=NF+1;++i){
        data[moduleName,statisticName,structFactorName_Value,statType]= data[moduleName,statisticName,structFactorName_Value,statType] " " $i
    }

    data[moduleName,statisticName,structFactorName_Value,statType]= data[moduleName,statisticName,structFactorName_Value,statType] "];"

    print data[moduleName,statisticName,structFactorName_Value,statType] > out_file
}

#complete each vector and print them into the output file
END{
    #print "parseVec:\tAll parse completed. Creation of "out_file
    print "parseVec:\t"out_file" created. Script completed"
}
