function [] = distributionStudy(path, statisticName, distribution)
addpath ../;
addpath ../plots/;

vectors = loadData(path,'v');
values = vectors.parsedVectors.(statisticName).rep_0(:,2);

if(strcmp(statisticName,'queueingTime'))
    histValues=values(find(values ~= 0));
    %histValues=values;
else
    histValues=values;
end
bucketLength=480;
cutValue=8000;
%bucketNumber=ceil(max(values)/bucketLength);

edges = (0:bucketLength:cutValue);
%histValues=histValues(find(histValues < cutValue));

fig = plotHistogram(histValues, strcat('EPDF',statisticName),distribution);
figure2file(fig,path);


end
