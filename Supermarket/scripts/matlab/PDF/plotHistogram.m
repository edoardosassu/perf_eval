function fig = plotHistogram( values,figureName, distribution)
fig=figure('Name', figureName);
set(fig, 'Visible', 'off');

hold on;
grid on;

histogram(values, 'Normalization','pdf');
%histogram(values);


xPdfValues=1:1:max(values);
pd=fitdist(values, distribution);
yPdfValues=pdf(pd,xPdfValues);
plot(xPdfValues,yPdfValues, 'LineWidth', 2);

hold off;

end
