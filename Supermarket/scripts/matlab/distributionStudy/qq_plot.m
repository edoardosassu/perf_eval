%The function calculate the theoretical distribution quantiles and plot the qqplot with the confidence interval. 
%Furthermore plot the regression line and print the regression function and the rsquared value.


function fig = qq_plot(values,percentiles,figureName,confidenceInterval,pd)

addpath ../;

fig=figure('Name',figureName);
title(figureName);
hold on;
set(fig, 'Visible', 'off');

values = sort(values);

pdQuantiles = icdf(pd,percentiles);
grid on;
e = errorbar(pdQuantiles,values,confidenceInterval(:,1));
e.MarkerEdgeColor='red';
e.MarkerSize=5;
e.Marker = '*';
e.LineStyle='none';


[angularCoeff,offset,rSquared] = getLinearRegression(pdQuantiles,values);
syms z positive;
y = z*angularCoeff + offset;
fplot(y,'--');


text(1,max(values),sprintf('Rsq = %g\ny = %gx+%g',rSquared,angularCoeff,offset));
legend('Empirical and Confidence interval','Theoretical','Location','southeast');

hold off;


end