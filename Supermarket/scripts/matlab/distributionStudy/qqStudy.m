function [] = qqStudy(dataPath,scenarioPath,statisticName,distribution)

addpath ../;
addpath ../plots/;
addpath ../confidenceInterval/;
addpath ../regression/;

numPercentiles = 500;

structOfVectors = loadData(dataPath);
vectors = structOfVectors.parsedVectors.(statisticName);

filterZeroes = false;
if(statisticName=='queueingTime')
    filterZeroes = true;
end

[meanQuantiles,confidenceInterval,percentiles] = getMeanQuantilesAllRepetitions(numPercentiles,vectors,filterZeroes);

pd=makedist(distribution);
figureName=strcat(statisticName,'Vs',distribution);
fig = qq_plot(meanQuantiles,percentiles,figureName,confidenceInterval,pd);

figure2file(fig,scenarioPath);
end
