%The function takes a structure composed by an array of statistic values for every repetitions.
%Generate the percentiles to study, for every repetition remove the zeros (if the statistic is queueingTime)
%and create a vector containing the quantiles
%Every vector will inserted in a matrix repetitionsQuantiles.
%The matrix has a row for every repetitions and a column for every quantiles.
%At the end calculate the mean and the confidence interval for every quantile/colomn and return them.


function [meanQuantiles,confidenceInterval,percentiles] = getMeanQuantilesAllRepetitions(numPercentiles,vectors, zeroFilter)

run('configVariables');
fieldNames = fieldnames(vectors);

percentiles = ((1:1:numPercentiles)-0.5)./numPercentiles;

repetitionsQuantiles = [];
n=numel(fieldNames);
for r=1:numel(fieldNames)
    repetitionValues = vectors.(fieldNames{r})(:,2);
    if(zeroFilter)
        repetitionValues=repetitionValues(find(repetitionValues>0));
    end
    repetitionsQuantiles = [repetitionsQuantiles ; arrayfun(@(p) prctile(repetitionValues,p*100),percentiles)];
end

confidenceInterval = [];
meanQuantiles = [];
for i=1:size(repetitionsQuantiles,2)
    [meanVal, CI]=getMeanWithCI(repetitionsQuantiles(:,i),CONFIDENCE_LEVEL);
    meanQuantiles = [meanQuantiles ; meanVal];
    confidenceInterval = [confidenceInterval ; CI];
end

end
