function [] = generateVectorsBin(dataPath)

run('configVariables')

data = loadData(dataPath);
[path,name,ext] = fileparts(dataPath);

cd (path);
save((name),'data');

clear;
end
