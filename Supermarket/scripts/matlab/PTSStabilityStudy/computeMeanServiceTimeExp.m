function [serviceTimes] = computeMeanServiceTimeExp()
expMean=120;
expLambda=1/expMean;
objServiceTime=3;
paymentTime= 30;

objectNumber=5:1:20;

fun = @(x) x.*expLambda.*exp(-expLambda.*x);

fastTillthresholds=paymentTime+(objectNumber*objServiceTime);

fastTillprobabilities = expcdf(fastTillthresholds,expMean);
standardTillprobabilities = 1-fastTillprobabilities;

indexes=1:1:numel(fastTillthresholds);

ftst=arrayfun(@(i)integral(fun,0,fastTillthresholds(i))./fastTillprobabilities(i),indexes);
stst=arrayfun(@(i)integral(fun,fastTillthresholds(i), Inf)./standardTillprobabilities(i),indexes);

serviceTimes =[ftst ; stst];


end

