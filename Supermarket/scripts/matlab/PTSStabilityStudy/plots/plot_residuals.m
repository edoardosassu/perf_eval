function [ residuals_fig ] = plot_residuals(mean_values, residuals_matrix, figure_name)
residuals_fig = figure('Name',figure_name);
title('Residuals Sctatter Plot');

set(residuals_fig, 'Visible', 'off');

grid on;
hold on;
for i=1:size(residuals_matrix,2)
    scatter(mean_values, residuals_matrix(:,i),'filled','o');
end
hold  off;
residuals_fig.CurrentAxes.YAxisLocation = 'origin';
residuals_fig.CurrentAxes.XAxisLocation = 'origin';
limits = [min(min(residuals_matrix))*1.5 max(max(residuals_matrix))*1.5];
if(~(isequal(limits,[0 0])))
   ylim(limits);
end


end
