function [coefficients] = getCoefficients(factorsValues)

coefficients=factorsValues;
for i=1:size(factorsValues,2)
   coefficients(factorsValues(:,i)==max(factorsValues(:,i)),i)=1;
   coefficients(factorsValues(:,i)==min(factorsValues(:,i)),i)=-1;
end
 
end

