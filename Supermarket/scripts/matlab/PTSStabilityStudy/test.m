clear;

addpath plots/;
addpath ../plots/;

run('./test_factors.m');

CI=0.95;
CHARTS_PATH = 'charts';

coeff = vectors.factors_coeff;
values = vectors.values;

[factors_weight, CI,  qi, SSX, SSE,mean_values, residuals] = analisys_2kr(coeff, values, CI);

figure2file(qq_plot(residuals,'2kr_qqplot'), CHARTS_PATH);
figure2file(plot_residuals(mean_values, residuals, '2kr_residuals'), CHARTS_PATH);

