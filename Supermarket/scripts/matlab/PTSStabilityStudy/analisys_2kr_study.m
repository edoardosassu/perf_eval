function [] = analisys2krStudy(path)
addpath plots/;
addpath ../plots/;
addpath ../;

vectors = loadData(path);

confidenceLevel = 0.95;

factorsValues = vectors.parsedStatistics2kr.factors_values;
statisticValues = vectors.parsedStatistics2kr.statistic_values;
coefficients = getCoefficients(factorsValues);

[factors_weight, CI,  qi, SSX, SSE,mean_values, residuals] = analisys_2kr(coefficients, statisticValues, confidenceLevel);

figure2file(qq_plot(residuals,'2kr_qqplot'), path);
figure2file(plot_residuals(mean_values, residuals, '2kr_residuals'), path);
