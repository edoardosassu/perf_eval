ZERO_FILTER=["queueingTime" "standardTillsQueueingTime" "fastTillsQueueingTime"];
NO_FILTER=["throughput"];
CONFIDENCE_LEVEL=0.95;
NUM_PERCENTILES= 150;
QT_MERGE=["queueingTime" "standardTillsQueueingTime" "fastTillsQueueingTime"]
RT_MERGE=["responseTime" "standardTillsResponseTime" "fastTillsResponseTime"]

%possible values:
%-incrementalMean
%-moveMean
WARMUP_MEAN_MODE='incrementalMean';
WINDOW_SIZE=80;



%Statistic type uset in plots. Possible values:
%-mean
%-noWaitPerc
STATISTIC_TYPE='mean';
%STATISTIC_TYPE='noWaitPerc';
%Constant to plot in statistic vs factors wit CI
%CONSTANT_TO_PLOT= [92.409109 1.021872]; %mean queueingTime
%CONSTANT_TO_PLOT_STAT_NAME = "UTS queueingTime";
%CONSTANT_TO_PLOT = [137.189160 1.148828]; %mean responseTime
%CONSTANT_TO_PLOT_STAT_NAME = "UTS responseTime";
%CONSTANT_TO_PLOT = [0.477076 0.020463]; %no wait percentage
%CONSTANT_TO_PLOT_STAT_NAME = "UTS no wait percentage";
