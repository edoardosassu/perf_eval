function [] = tillsFairnessStudy(dataPath,scenarioPath, statisticName, scenarioName, moduleName)
addpath ../;
addpath ../plots/;

run('configVariables');

dataPath=char(dataPath);
scenarioPath=char(scenarioPath);
statisticName=char(statisticName);
scenarioName=char(scenarioName);

if nargin < 5
  moduleName='Tills';
else
  moduleName=char(moduleName);
end

monuleNameRegex=strcat('.+',moduleName,'.+');

statisticsStruct=loadData(dataPath);
statisticsStruct=statisticsStruct.parsedStatistics;
modulesName = fieldnames(statisticsStruct);

validModules=[];
for i=1:numel(modulesName)
    match=regexp(modulesName{i},monuleNameRegex,'match');
    if(numel(match)>0)
        validModules=[validModules modulesName(i)];
    end
end

tillsStatValues=[];
for i=1:numel(validModules)
    tillName=char(validModules(i));
    tillStat=statisticsStruct.(tillName).(statisticName).(scenarioName).(STATISTIC_TYPE);
    %tillStat is a vector with this format [statValue, statCI]
    tillsStatValues=[tillsStatValues; tillStat(:,1)];
end

figureName=strcat("Tills ",statisticName," Lorenz curve");
fig=lorenzCurvePlot(tillsStatValues,figureName);
figure2file(fig, scenarioPath);

end
