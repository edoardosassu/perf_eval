function [fig] = lorenzCurvePlot(data, figureName)
data = sort(data);
total = sum(data);
n = numel(data);

fig=figure('Name', figureName);
set(fig, 'Visible', 'off');

hold on;
grid on;

%plot lorenz curve and bisector in the chart
x = ((1:1:n)')./n;
x=[0;x];
y = cumsum(data)./total;
y=[0;y];
xx=0:0.01:1;
yy=interp1(x,y,xx,'linear');

%plot([0;x],[0;y],[0;1],[0;1]);
plot([0,1],[0,1]);
plot(xx,yy);

xlim([0 1]);
ylim([0 1]);

valMean=mean(data);
MAD=1/numel(data)*sum(abs(data-valMean));
LCG=(MAD)/(2*valMean);
text(0.1,0.9,sprintf('LCG = %g\n',LCG));

hold off;
end

