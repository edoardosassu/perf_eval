function [ filteredVector ] = outlayerFilter(vec,dimension)

Q1=prctile(vec(:,dimension),25);
Q3=prctile(vec(:,dimension),75);

if((Q3-Q1)>0)
    maxLimit=Q3+((Q3-Q1)*1.5);
    minLimit=Q1-((Q3-Q1)*1.5);
    
    vec=vec(find(vec(:,dimension)<=(maxLimit)),:);
    vec=vec(find(vec(:,dimension)>=(minLimit)),:);
end

filteredVector=vec;
end

