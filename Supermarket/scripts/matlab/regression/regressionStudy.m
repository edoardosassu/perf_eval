function regressionStudy(dataPath, scenarioPath, statisticName, regressionType)
%try
    addpath ../;
    addpath ../plots/;
    
    confidenceLevel = 0.95;
    
    data = loadData(dataPath); 
    structOfVectors = data.parsedStatistics.Supermarket.(statisticName);
   
    statisticValues = [];
    factorValues = [];
    
    fieldNames = fieldnames(structOfVectors);
    factorName='';
    
    for r=1:numel(fieldNames)
        variableDataStruct = structOfVectors.(fieldNames{r});
        if(strcmp(factorName,''))
            vdsFielsNames=fieldnames(variableDataStruct);
            factorName=vdsFielsNames{1};
        end
        factorValues = [factorValues; variableDataStruct.(factorName)];
        statisticValues = [statisticValues; mean(variableDataStruct.mean)];
    end
        
    %order according to factor values    
    temp = sortrows([factorValues statisticValues],1);
    factorValues = temp(:,1);
    statisticValues = temp(:,2);
    
    switch regressionType
    case 'linear' 
        fig = plotLinearRegression(factorValues,statisticValues,confidenceLevel,'Linear regression:'+statisticName+'_vs_'+factorName);
    
    case 'logLinear'
        [positiveX,transformedY] = logTransform(factorValues,statisticValues);
        fig = plotLinearRegression(positiveX,transformedY,confidenceLevel,'Linear regression:log('+statisticName+')_vs_'+factorName);
    
    case 'exponential'
        fig = plotExponentialRegression(factorValues,statisticValues,confidenceLevel,'Exponential regression:'+statisticName+'_vs_'+factorName);
    
    otherwise
        warning('Unexpected regression type');
    end
    
    figure2file(fig,scenarioPath);
    
%catch
%    clear;
%end
end
