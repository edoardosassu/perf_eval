function [expRegression,lowCI,highCI] = getExpRegression(x,y,confidenceLevel)

addpath ../;

[positiveX, transformedY] = logTransform(x,y);

%Linear Fit
[angularCoeff,offset,rSquared,CI] = getLinearRegression(positiveX,transformedY,confidenceLevel);

%antitransform
syms z; %crea variabile simbolica
expRegression = exp(offset)*exp(angularCoeff*z);
lowCI = exp(CI(1,1))*exp(CI(2,1)*z);
highCI = exp(CI(1,2))*exp(CI(2,2)*z);

%plot
% grid on;
% hold on;
% plot(x,y);
% fplot(exponentialRegression);
% fplot(lowerCI,"--r");
% fplot(higherCI,"--r");
% str = { ['y = ' num2str(beta(1)) '*x' num2str(beta(2))]};
% 
% legend('y = data','y = fit','y = confidence bounds','Location','northwest')
% hold off;
% 
% annotation('textbox','String',str,'FitBoxToText','on');

end