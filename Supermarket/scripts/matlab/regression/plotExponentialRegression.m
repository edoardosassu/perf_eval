function [fig] = plotExponentialRegression(x,y,confidenceLevel,figureName)

fig=figure('Name', figureName);
title(strrep(figureName,'_',' '));
set(fig, 'Visible', 'off');

hold on;
grid on;

%plot x and y
p = plot(x,y);
p.Marker = '*';
p.LineStyle = 'none';

%plot regression with confidence interval
[expRegression,lowCI,highCI] = getExpRegression(x,y,confidenceLevel);
fplot(expRegression);
fplot(lowCI,'r--');
fplot(highCI,'r--');

charFunction = char(expRegression);
text(1,80,sprintf('y = %g',charFunction));

xlim([0 max(x)*1.5]);
ylim([0 max(y)*1.1]);

hold off;

end
