function [fig] = plotLinearRegression(x,y,confidenceLevel,figureName)

fig=figure('Name', figureName);
title(strrep(figureName,'_',' '));
set(fig, 'Visible', 'off');

hold on;
grid on;

[angularCoeff,offset,rSquared,CI] = getLinearRegression(x,y,confidenceLevel);

%plot x and y
p = plot(x,y);
p.Marker = '*';
p.LineStyle = 'none';

%plot regression line and coefficient interval
syms z positive;
regressionFunction = z*angularCoeff + offset;
lowCI = CI(1,1)+z*CI(2,1);
highCI = CI(1,2)+z*CI(2,2);
fplot(regressionFunction ,'--');
fplot(lowCI,'--r');
fplot(highCI,'--r');

text(1,80,sprintf('R^2 = %g\ny = %gx+%g',rSquared,angularCoeff,offset));
% t.Units='normalized';
% legend('Linear regression','Low confidence interval','High confidence interval','southeast');

xlim([0 max(x)*1.5]);
ylim([0 max(y)*1.1]);

hold off;

end
