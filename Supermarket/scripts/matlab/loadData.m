% mode can be:

function ret = loadData(dataPath)

[path, name, ext] = fileparts(dataPath);

if(strcmp(ext,'.mat'))
    load(dataPath);
    ret=data;
else
    run(dataPath);
    ret.(name)=struct;
    ret.(name)=data;
end

clear 'ans';
end
