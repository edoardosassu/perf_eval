function [] = filterVectorsOutlayer(dataPath,factorValue, enableMerge)

addpath ./confidenceInterval/

run('configVariables')

data = loadData(dataPath);
[path,name,ext] = fileparts(dataPath);

statFilePath=fullfile(path,'noWaitPerc.stats');

[fileID, error]=fopen(statFilePath,'w');

modules=fieldnames(data.parsedVectors);

for k=1:numel(modules)
    moduleName=modules{k};
    
    if(isstruct(data.parsedVectors.(moduleName)))
        
        statistics=fieldnames(data.parsedVectors.(moduleName));
        for i=1:numel(statistics)
            stat=statistics{i};
            
            if(~ismember(stat,NO_FILTER))
                
                if(ismember(stat,ZERO_FILTER))
                    noWaitPercs=[];
                end
                
                repetitions=fieldnames(data.parsedVectors.(moduleName).(stat));
                
                for j=1:numel(repetitions)
                    
                    rep=repetitions{j};
                    repData=data.parsedVectors.(moduleName).(stat).(rep);
                    
                    if(ismember(stat,ZERO_FILTER))
                        noWaitPercs=[noWaitPercs; sum(repData(:,2)==0)/numel(repData(:,2))];
                        repData=repData(find(repData(:,2)>0),:);
                    end
                    
                    filteredData=outlayerFilter(repData,2);
                    data.parsedVectors.(moduleName).(stat).(rep)=filteredData;
                end
                
                if(ismember(stat,ZERO_FILTER))
                    [statMean,statCI]=getMeanWithCI(noWaitPercs,CONFIDENCE_LEVEL);
                    fprintf(fileID,'%s %s %s noWaitPerc %f %f\n', factorValue, moduleName, stat, statMean, statCI);
                end
            end
            
            
        end
    end
end

if(enableMerge == true)
    for k=1:numel(modules)
        moduleName=modules{k};
        
        if(isstruct(data.parsedVectors.(moduleName)))
            qt_statistic=char(QT_MERGE(1));
            ftqt_statistic=char(QT_MERGE(2));
            stqt_statistic=char(QT_MERGE(3));
            
            rt_statistic=char(RT_MERGE(1));
            ftrt_statistic=char(RT_MERGE(2));
            strt_statistic=char(RT_MERGE(3));
            
            repetitions=fieldnames(data.parsedVectors.(moduleName).(qt_statistic));
            for j=1:numel(repetitions)
                rep=repetitions{j};
                stqt_repData=data.parsedVectors.(moduleName).(stqt_statistic).(rep);
                ftqt_repData=data.parsedVectors.(moduleName).(ftqt_statistic).(rep);
                strt_repData=data.parsedVectors.(moduleName).(strt_statistic).(rep);
                ftrt_repData=data.parsedVectors.(moduleName).(ftrt_statistic).(rep);
                
                data.parsedVectors.(moduleName).(qt_statistic).(rep)=[ftqt_repData; stqt_repData];
                data.parsedVectors.(moduleName).(rt_statistic).(rep)=[ftrt_repData; strt_repData];
            end
            
        end
    end
end

fclose(fileID);

cd (path);
name=strcat('filtered_',name);
save((name),'data');

clear;
end
