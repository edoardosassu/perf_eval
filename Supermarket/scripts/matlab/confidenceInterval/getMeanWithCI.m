function [meanValue,CI] = getMeanWithCI(values,confidenceLevel)
values = sort(values);
%lowCI = prctile(values,(1-confidenceLevel)*100);
%highCI = prctile(values,confidenceLevel*100);
%confidenceInterval = [values-lowCI highCI-values];

sampleStd = std(values);
sqrtNumel= sqrt(numel(values));
alphaHalf=abs(norminv((1-confidenceLevel)/2));

CI = sampleStd*alphaHalf/sqrtNumel;
meanValue=mean(values);

end
