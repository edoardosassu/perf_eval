#!/bin/bash
CURRENT_SCRIPT_FOLDER_PATH=$(readlink -f $(dirname $0))
PROJECT_PATH=$(echo $CURRENT_SCRIPT_FOLDER_PATH | awk '{split($1,path,"/Supermarket"); print path[1];}')
SUPERMARKET_PATH=$PROJECT_PATH/Supermarket
SCRIPTS_PATH=${SUPERMARKET_PATH}/scripts
AWK_PATH=${SCRIPTS_PATH}/awk
SIMULATION_SCRIPTS_PATH=${SUPERMARKET_PATH}/scripts/simulationScripts

IS_SIMULATION_ON=0
GENERATE_STATS=0
GENERATE_VEC_BIN=0
THREAD_NUM=2
DELETE_FILES=0
SIMULATION_MODE=""
VECTOR_MERGE=0

while getopts m:w:srbMh option; do
    case ${option} in
        m) IS_SIMULATION_ON=1
        SIMULATION_MODE="-m"
        THREAD_NUM=$OPTARG;;
        w) IS_SIMULATION_ON=1
        SIMULATION_MODE="-w"
        THREAD_NUM=$OPTARG;;
        s) GENERATE_STATS=1;;
        b) GENERATE_VEC_BIN=1;;
        M) VECTOR_MERGE=1;;
        r) DELETE_FILES=1;;
        h)
        echo -e "\nAvailable options are:"
        echo "-m [number of threads]: run measurament simulation";
        echo "-w [number of threads]: run warmup simulation";
        echo "-s: generate parsedStatistics.m";
        echo "-b: generate vectors bin"
        echo "-M: enable vector merge in generateMatlabBin"
        echo "-r: remove .vec, .vci and .mat files after -B command";
        exit 1;;
        \?)
        exit 1;;
    esac
done


if [ $IS_SIMULATION_ON -eq 1 ]
then
    ${SIMULATION_SCRIPTS_PATH}/parallelize_simulations "${SIMULATION_SCRIPTS_PATH}/run_simulation ${SIMULATION_MODE}" $THREAD_NUM ./
fi

if [ $GENERATE_VEC_BIN -eq 1 ]
then
    $SIMULATION_SCRIPTS_PATH/generateMatlabBin ${CURRENT_SCRIPT_FOLDER_PATH} parsedVectors $VECTOR_MERGE

    #$SIMULATION_SCRIPTS_PATH/delete_files -vVim -f ${CURRENT_SCRIPT_FOLDER_PATH}
fi

if [ $DELETE_FILES -eq 1 ]
then
    ${SIMULATION_SCRIPTS_PATH}/delete_files -vVm -f ${CURRENT_SCRIPT_FOLDER_PATH} > /dev/null
fi

if [ $GENERATE_STATS -eq 1 ]
then
    statsCount=$(ls -1 ${CURRENT_SCRIPT_FOLDER_PATH}/*/*.stats 2>/dev/null | wc -l)
    if [ ${statsCount} -gt 0 ]
    then
        FACTOR_NAME=$(basename $CURRENT_SCRIPT_FOLDER_PATH | awk '{ print gensub(/[Ss]tudy/, "", "g", $0);}')
        echo "=====================Parse .stats results======================="
        awk -v out=${CURRENT_SCRIPT_FOLDER_PATH}/parsedStatistics -v factorName=${FACTOR_NAME} -f ${AWK_PATH}/parse_scenario_scalar.awk ${CURRENT_SCRIPT_FOLDER_PATH}/*/*.stats > /dev/null
    fi
fi
