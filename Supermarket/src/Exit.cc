//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
// 

#include "Exit.h"

Define_Module(Exit);

void Exit::initialize()
{
    this->meanClientCreationDelay = par("meanClientCreationDelay");
    this->throughputSignal = registerSignal(par("throughputSignal"));
    this->servedClientsCounter = 0;

    this->emitStatisticMsg = new cMessage("Emit Statistic");

    scheduleAt(simTime()+this->meanClientCreationDelay, this->emitStatisticMsg);
}

void Exit::handleMessage(cMessage *msg)
{
    if(msg->isSelfMessage()){
        this->emitStatistic();
    }
    else{
        this->handleClientDeparture();
        cancelAndDelete(msg);
    }
}

/*
 * This method counts the number of served client every meanClientCreationDelay seconds.
 * If the system is stable, the throughput computed in this way must be 1.
*/
void Exit::emitStatistic(){

    emit(this->throughputSignal, this->servedClientsCounter);

    this->servedClientsCounter = 0;
    scheduleAt(simTime()+this->meanClientCreationDelay, this->emitStatisticMsg);
}

void Exit::handleClientDeparture(){
    this->servedClientsCounter += 1;
}

void Exit::finish(){
    this->cancelEvent(this->emitStatisticMsg);
    cancelAndDelete(this->emitStatisticMsg);
}
