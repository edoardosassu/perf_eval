//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
// 

#include "ClientCreator.h"
#include "Client_m.h"

Define_Module(ClientCreator);

void ClientCreator::initialize()
{
    this->meanCreationDelay = par("meanClientCreationDelay");
    this->fastTillProductLimit = par("fastTillProductLimit");
    this->isFastTillScenario = par("isFastTillScenario");
    this->productServiceTime = par("productServiceTime");
    this->paymentTime = par("paymentTime");


    this->fastTillServiceTimeLimit = (this->fastTillProductLimit*this->productServiceTime)+this->paymentTime;
    this->createClienteMsg = new cMessage("createNewClient");

    if(par("debugMode")){
        std::cout<<"fastTillServiceTimeLimit-->"<<this->fastTillServiceTimeLimit<<std::endl;
        std::cout<<"meanCreationDealy-->"<<meanCreationDelay<<std::endl;
    }

    this->scheduleNextClient();
}

void ClientCreator::handleMessage(cMessage *msg)
{
    if(msg->isSelfMessage()){
        this->sendNewClient();
        this->scheduleNextClient();
    }
}

void ClientCreator::scheduleNextClient(){
    double creationDelay=exponential(this->meanCreationDelay,0);
    scheduleAt(simTime()+creationDelay, this->createClienteMsg);

    if(par("debugMode")){
        std::cout<<"clientCreationDelay-->"<<creationDelay<<std::endl;
    }

}

void ClientCreator::sendNewClient(){
    Client* newClient = new Client("Client");
    newClient->setServiceTime(par("serviceTime"));

    /*
     * If the clientServiceTime < = fastTillServiceTimeLimit it means that the client has
     * a number of product lower than fastTillProductLimit and it can go to the priority tills
     */
    bool isFastTillClient = newClient->getServiceTime() <= this->fastTillServiceTimeLimit;

    if(par("debugMode")){
        std::cout<<"ServiceTime-->"<<newClient->getServiceTime()<<std::endl;
    }

    if(this->isFastTillScenario && isFastTillClient){
        send(newClient,"fastTillSwitcher");
    }
    else{
        send(newClient,"standardTillSwitcher");
    }
}

void ClientCreator::finish(){
    this->cancelEvent(this->createClienteMsg);
    cancelAndDelete(this->createClienteMsg);


}
