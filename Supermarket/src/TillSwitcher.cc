//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
// 

#include "TillSwitcher.h"
#include <algorithm>
#include <iostream>

Define_Module(TillSwitcher);

/*
 * Comparator functor used to build the tills min heap
 */
TillSwitcher::Comparator::Comparator(const TillSwitcher* t):tillSw(t){}

bool TillSwitcher::Comparator::operator()(const int& g1, const int& g2) const{

    const int g1Clients = this->tillSw->tillReferences->at(g1)->getNumberOfClient();
    const int g2Clients = this->tillSw->tillReferences->at(g2)->getNumberOfClient();
    return g1Clients>g2Clients;
}

void TillSwitcher::initialize(int stage)
{
    if(stage == 0){
        this->numberOfTills=par("numberOfTills");
        this->tillReferences=new std::vector<Till*>();
        this->minHeap=new std::vector<int>();

        if(par("debugMode")){
            std::cout<<"number of tills-->"<<this->numberOfTills<<std::endl;
        }

    }
    else if(stage == 1){
        this->createMinHeap();
    }

}

void TillSwitcher::handleMessage(cMessage *msg)
{
    if(strcmp(msg->getName(),"Client")==0)
    {
        Client* nextClient = check_and_cast<Client* >(msg);
        tillSwitching(nextClient);
    }
}

void TillSwitcher::createMinHeap()
{
    for(int i=0;i<this->numberOfTills;i++)
    {
        Till *tillReference =
                check_and_cast< Till* >(gate("tillOut",i)->getNextGate()->getOwnerModule());
        tillReferences->push_back(tillReference);
        minHeap->push_back(i);
    }
}

void TillSwitcher::tillSwitching(Client *nextClient)
{
    if(par("validationMode")){
        /*
         * In validation mode every till has the same probability to be selected
         * This was used for the validation of the simulator
         */
        int index=intuniform(0,this->tillReferences->size()-1);
        sendNextClient(index,nextClient);
    }
    else{
        /*
         * Otherwise a till is selected if is the till with the lowest number of client
        */
        Comparator myComp(this);
        std::make_heap(this->minHeap->begin(),this->minHeap->end(),myComp);
        if(par("debugMode")){
            this->printTillsStatus();
        }
        sendNextClient(minHeap->front(),nextClient);
    }


}

void TillSwitcher::printTillsStatus(){
    for(unsigned int i=0;i<this->tillReferences->size();++i){
        int nClients = this->tillReferences->at(i)->getNumberOfClient();
        std::cout<<"till "<<i<<": "<<nClients<<endl;
    }
}

void TillSwitcher::sendNextClient(int gate, Client* nextClient){
    if(par("debugMode")){
        std::cout<<"till "<<gate<<"selected.---service time: "<<nextClient->getServiceTime()<<endl;
    }
    send(nextClient,"tillOut",gate);
}

void TillSwitcher::finish(){
    delete this->tillReferences;
    delete this->minHeap;
}
