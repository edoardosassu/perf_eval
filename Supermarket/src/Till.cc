//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
//

#include <queue>
#include <string>
#include <vector>
#include "Till.h"
#include "Client_m.h"

Define_Module(Till);

/*
 * parseSignals take a string of signals names divided by spaces and register a simsignal_t for each
 * signalName
*/
std::vector<simsignal_t>* Till::parseSignals(const std::string signals){
    std::vector<simsignal_t> *signalsVector = new std::vector<simsignal_t>();

    std::stringstream stream(signals);
    std::string segment;
    while(std::getline(stream, segment, ' '))
    {
        signalsVector->push_back(registerSignal(segment.c_str()));
    }

    return signalsVector;
}

/*
 * emitSignals take a vector of simsignal_t and emit each signal in the vector
 * This is useful when two or more signal with the same value must be sent when an event occurs
*/
void Till::emitSignals(std::vector<simsignal_t>* signals, const double value){
    for (std::vector<simsignal_t>::iterator i=signals->begin(); i!=signals->end();++i){
        simsignal_t currentSignal = *i;
        emit(currentSignal, value);
    }
}

void Till::initialize(int stage)
{
    if(stage==0){
        this->tillQueue = new std::queue<Client*>();
        this->servedClient = new cMessage("Cliente servito");
        this->busy=false;

        this->queueingTimeSignals = this->parseSignals(par("queueingTimeSignals").stringValue());
        this->responseTimeSignals = this->parseSignals(par("responseTimeSignals").stringValue());
        this->numberOfClient = registerSignal(par("numberOfClientSignal"));
    }
}

void Till::handleMessage(cMessage *msg)
{
    bool isServedClientMessage = msg->isSelfMessage();

    if(isServedClientMessage){
        this->notifyServedClient();
        this->handleNextClient();
    }

    else if(strcmp(msg->getName(),"Client")==0){
        this->handleNewClient(msg);
    }
}

void Till::handleNextClient(){

    if(this->tillQueue->size() > 0){
        Client* nextClient = this->tillQueue->front();
        this->tillQueue->pop();

        this->serveClient(nextClient);
    }
    else{
        this->busy=false;
    }
}

void Till::handleNewClient(cMessage *msg){
    if(this->tillQueue->size() == 0 && !this->busy){
        this->busy = true;
        Client* nextClient = check_and_cast<Client *>(msg);
        this->serveClient(nextClient);
    }
    else{
        this->tillQueue->push(check_and_cast<Client *>(msg));
    }
    emit(numberOfClient,(int)tillQueue->size());
}

void Till::notifyServedClient(){
    send(new cMessage("served client"), "out");
}

void Till::serveClient(Client *c){

    const simtime_t arrivalTime = c->getCreationTime();
    double serviceDelay = c->getServiceTime();


    double queueingTimeVal=(simTime()-arrivalTime).dbl();
    double responseTimeVal=(queueingTimeVal+serviceDelay);

    this->emitSignals(this->queueingTimeSignals, queueingTimeVal);
    this->emitSignals(this->responseTimeSignals, responseTimeVal);

    scheduleAt(simTime()+serviceDelay, this->servedClient);
    cancelAndDelete(c);
}

const int Till::getNumberOfClient()
{
    int numberOfClient = ((int)this->busy + this->tillQueue->size());

    if(par("debugMode")){
        std::cout<<"till queue: "<<this->tillQueue->size()<<" busy: "<<this->busy<<std::endl;
    }

    return numberOfClient;
}

void Till::finish(){
    this->cancelEvent(this->servedClient);
    cancelAndDelete(this->servedClient);

    while(this->tillQueue->size() != 0){
        cancelAndDelete(this->tillQueue->front());
        this->tillQueue->pop();
    }

    delete this->tillQueue;
    delete this->queueingTimeSignals;
    delete this->responseTimeSignals;
}
